#!/bin/bash
# The third shell script
# Jonas Schäufler & Philipp Prögel
# 02.04.2012


SIZE=10
SIZESTR=""
ISVERBOSE=""
ISTXT=0

usage(){
  cat << EOF
./splitfix.sh --help
  splitfix.sh [OPTIONS] FILE [FILE ...] Split FILE into
    fixed-size pieces.
    The pieces are 10 lines long,
      if FILE is a text file.
    The pieces are 10 kiB long,
      if FILE is *not* a text file.
    The last piece may be smaller, it contains the rest
      of the original file.
    The output files bear the name of the input file
      with a 4-digit numerical suffix.
    The original file can be reconstructed with
      the command       ‘‘cat FILE.*’’

  EXAMPLE:
      splitfix.sh foo.pdf
        splits foo.pdf into the files
        foo.pdf.0000 foo.pdf.0001 etc.

  splitfix.sh [-h | --help] This help text
  splitfix.sh --version         Print version number

    --help      this help text
  OPTIONS:
  -h

  -s SIZE       size of the pieces
                  in lines (for text files)
                  or in kiBytes (for other files)

  -v
    --verbose print debugging messages
EOF
}


if [ $# -lt 1 ] # wenn Parameteranzahl less than 1
then
  echo "Too less parameters"
  exit 1
fi


while [ $# -ge 1 ]  # wenn Parameteranzahl greater/equal 1
do
  case $1 in

  "-s")						# -s
  if [ $# -lt 2 ]
   then
         echo "Invalid parameters"
   else
         shift 1 # nächster Parameter
         SIZE=$1  # größe setzten
   fi
 ;;


  "-v" | "--verbose")		# -v
    ISVERBOSE="--verbose" # Verbose Flag setzten
    ;;

   "-h" | "--help")			# -h
      usage # helpText ausgeben
   ;;

   *) 						# default

    ISTXT=`file $1 | grep -o txt` # Dateiendung prüfen

    case $ISTXT in
    "txt") # wenn die Dateiendung txt ist
      SIZESTR="--lines=$SIZE" # Parameter lines 10
      ;;
    *)
      SIZETMP=`expr $SIZE \* 1024` # ansonsten size in bytes umrechnen
      SIZESTR="--bytes=$SIZETMP" # Parameter bytes 
      ;;
    esac

    
    split -d -a 4 $ISVERBOSE $SIZESTR $1 $1. # split aufrufen
    ;;
  esac

   shift 1
done

exit 0
-- VISUAL --